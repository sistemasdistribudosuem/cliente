package cliente;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {
	
	static String dns;
	
	public static void main(String[] args){
		validaParametros();
		
		gravaDNS();
		
		executaCliente();
	}

	private static void executaCliente() {
		System.out.println("Cliente sendo iniciado!");
		System.out.println("Digite comandos no formato:\n"
				+ "<servico>  <parametro1> <parametro2> ...\n"
				+ "Para finalizar, digite 'fim'");
		
		
		Scanner sc = new Scanner(System.in);
		
		String comando = sc.nextLine();
		while(!"fim".equals(comando)){
			try{
				comando = comando.trim();
				String[] parametros = comando.split(" ");
				String servico = parametros[0].trim();
				
				String endereco = pesquisaServico(servico);
				
				String[] parametrosDoServico = getParametrosDoServico(parametros);
				
				System.out.println("Executando servico no endereco:"+endereco);
				
				String resultado = executaServico(endereco, parametrosDoServico);
				
				System.out.println(resultado);
			
			}
			catch (Exception e) {
				e.printStackTrace();
				System.out.println("Erro ao executar o comando");
			}
			comando = sc.nextLine();
		}
		sc.close();
	}
	
	private static String[] getParametrosDoServico(String[] parametros) {
		if(parametros.length < 2){
			System.out.println("Parametros nao informados");
			return null;
		}
		
		List<String> list = new ArrayList<String>(Arrays.asList(parametros));
		list.remove(0);
		
		String[] params = new String[list.size()];
		for(int i = 0;i<list.size();i++)
			params[i] = list.get(i);
		
		return params;
	}

	private static String executaServico(String endereco, String ...parametros ) {
		String urlStr = montaURL(endereco, parametros);
		
		try{
			StringBuilder result = new StringBuilder();
			
		    URL url = new URL(urlStr);
		    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		    conn.setRequestMethod("GET");
		    BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		    String line;
		    while ((line = rd.readLine()) != null) {
		       result.append(line);
		    }
		    rd.close();
		    String response = result.toString();
		    return response;
		}
		catch (Exception e) {
			return "Erro ao executar - "+urlStr;
		}
	}

	private static String montaURL(String endereco, String[] parametros) {
		StringBuilder builder = new StringBuilder();
		
		if(parametros != null && parametros.length > 0){
			for(int i = 0; i < parametros.length - 1; i++){
				String param = parametros[i];
				builder.append("par").append(param).append("=").append(param).append("&");
			}
			builder.append("par").append(parametros[parametros.length-1]).append("=").append(parametros[parametros.length-1]);
		}
		return String.format("http://%s/?%s", endereco, builder.toString());	
	}

	private static String pesquisaServico(String servico) {
		try{
			StringBuilder result = new StringBuilder();
			
			String urlStr = String.format("http://%s/pesquisa?nome=%s", dns, servico);
			
		    URL url = new URL(urlStr);
		    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		    conn.setRequestMethod("GET");
		    BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		    String line;
		    while ((line = rd.readLine()) != null) {
		       result.append(line);
		    }
		    rd.close();
		    String response = result.toString();
		    return response;
		}
		catch (Exception e) {
			e.printStackTrace();
			return "Erro ao pesquisar endereco";
		}
	}

	private static void gravaDNS() {
		dns = getIpDns()+":"+getPorta();
	}

	private static void validaParametros() {
		if(getIpDns() == null)
			throw new RuntimeException("Informe um DNS para o cliente (-Ddns=ip)");
		
		if(getPorta() == null)
			throw new RuntimeException("Informe uma porta para o DNS (-portaDns=porta)");
	}
	
	private static String getIpDns() {
		return System.getProperty("dns");
	}
	
	private static String getPorta() {
		return System.getProperty("portaDns");
	}

}
